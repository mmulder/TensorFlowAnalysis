import tensorflow as tf
import numpy as np
import itertools

# Use double precision throughout
fptype = tf.float64

# Sum of the list components
def Sum( ampls ) : return tf.add_n( ampls )

# Density for a complex amplitude
def Density(ampl) : return tf.abs(ampl)**2

# Create a complex number from real and imaginary parts
def Complex(re, im) : return tf.complex(re, im)

# Declare constant
def Const(c) : return tf.constant(c, dtype=fptype)

# Declare invariant
def Invariant(c) : return tf.constant([c], dtype=fptype)

# Sqrt
def Sqrt(x) : return tf.sqrt(x)

# Exp
def Exp(x) : return tf.exp(x)

# Sin
def Sin(x) : return tf.sin(x)

# Cos
def Cos(x) : return tf.cos(x)

# Asin
def Asin(x) : return tf.asin(x)

# Acos
def Acos(x) : return tf.acos(x)

# Tanh
def Tanh(x) : return tf.tanh(x)

# Pi
def Pi() : return Const( np.pi )

# Create a tensor with zeros of the same shape as input tensor
def Zeros(x) : return tf.zeros_like(x)

# Create a tensor with ones of the same shape as input tensor
def Ones(x) : return tf.ones_like(x)

#def Atan2(x, y) : 
#  r = Sqrt(x**2 + y**2)
#  phi = Acos(x/r)
#  s = tf.sign(y)
#  s2 = 1. - tf.abs(s) + s
#  phi2 = s2*phi + Pi()*(1-s2)

# Atan2 is not defined in TensorFlow, use own implementation
def Atan2(y, x):
  angle = tf.where(tf.greater(x,0.0), tf.atan(y/x), tf.zeros_like(x))
  angle = tf.where(tf.logical_and(tf.less(x,0.0),  tf.greater_equal(y,0.0)), tf.atan(y/x) + Pi(), angle)
  angle = tf.where(tf.logical_and(tf.less(x,0.0),  tf.less(y,0.0)), tf.atan(y/x) - Pi(), angle)
  angle = tf.where(tf.logical_and(tf.equal(x,0.0), tf.greater(y,0.0)), 0.5*Pi() * tf.ones_like(x), angle)
  angle = tf.where(tf.logical_and(tf.equal(x,0.0), tf.less(y,0.0)), -0.5*Pi() * tf.ones_like(x), angle)
  angle = tf.where(tf.logical_and(tf.equal(x,0.0), tf.equal(y,0.0)), np.nan * tf.zeros_like(x), angle)
  return angle

def Interpolate(t, c) : 
  """
    Multilinear interpolation on a rectangular grid of arbitrary number of dimensions
      t : TF tensor representing the grid (of rank N)
      c : Tensor of coordinates for which the interpolation is performed
      return: 1D tensor of interpolated values
  """
  rank = len(t.get_shape())
  ind = tf.cast(tf.floor(c), tf.int32)
  t2 = tf.pad(t, rank*[[1,1]], 'SYMMETRIC')
  wts = []
  for vertex in itertools.product([0, 1], repeat = rank) : 
    ind2 = ind + tf.constant(vertex, dtype = tf.int32)
    weight = tf.reduce_prod(1. - tf.abs(c - tf.cast(ind2, dtype = fptype)), 1)
    wt = tf.gather_nd(t2, ind2+1)
    wts += [ weight*wt ]
  interp = tf.reduce_sum(tf.stack(wts), 0)
  return interp

def SetSeed(seed) : 
  """
    Set random seed for numpy
  """
  np.random.seed(seed)
