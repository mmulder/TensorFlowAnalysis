from Interface import *
from Kinematics import *

def HelicityAmplitude(x, spin) : 
  """
  Helicity amplitude for a resonance in scalar-scalar state
    x    : cos(helicity angle)
    spin : spin of the resonance
  """
  if spin == 0 : return Complex( Const(1.), Const(0.))
  if spin == 1 : return Complex( x, Const(0.))
  if spin == 2 : return Complex( (3.*x**2-1.)/2., Const(0.))
  if spin == 3 : return Complex( (5.*x**3-3.*x)/2., Const(0.))
  if spin == 4 : return Complex( (35.*x**4-30.*x**2+3.)/8., Const(0.))
  return None

def RelativisticBreitWigner(m2, mres, wres) : 
  """
  Relativistic Breit-Wigner 
  """
  return 1./Complex(mres**2-m2, -mres*wres)

def BlattWeisskopfFormFactor(q, q0, d, l) : 
  """
  Blatt-Weisskopf formfactor for intermediate resonance
  """
  z  = q*d
  z0 = q0*d
  if l == 0 : return Const(1.)
  if l == 1 : return Sqrt((1+z0**2)/(1+z**2))
  if l == 2 : return Sqrt((9+3*z0**2+z0**4)/(9+3*z**2+z**4))
  if l == 3 : return Sqrt((225+45*z0**2+6*z0**4+z**6)/(225+45*z**2+6*z**4+z**6))
  if l == 4 : return Sqrt(((z0**2-45.*z0+105.)**2)+25.*z0*(2.*z0-21)/((z0**2-45.*z0+105.)**2)+25.*z*(2.*z-21))

def MassDependentWidth(m, m0, gamma0, p, p0, ff, l) : 
  """
  Mass-dependent width for BW amplitude
  """
  return gamma0*((p/p0)**(2*l+1))*(m0/m)*(ff**2)

def OrbitalBarrierFactor(p, p0, l) : 
  """
  Orbital barrier factor
  """
  return (p/p0)**l

def BreitWignerLineShape(m2, m0, gamma0, ma, mb, mc, md, dr, dd, lr, ld, barrierFactor = True) : 
  """
  Breit-Wigner amplitude with Blatt-Weisskopf formfactors, mass-dependent width and orbital barriers
  """
  m = Sqrt(m2)
  q  = TwoBodyMomentum(md, m, mc)
  q0 = TwoBodyMomentum(md, m0, mc)
  p  = TwoBodyMomentum(m, ma, mb)
  p0 = TwoBodyMomentum(m0, ma, mb)
  ffr = BlattWeisskopfFormFactor(p, p0, dr, lr)
  ffd = BlattWeisskopfFormFactor(q, q0, dd, ld)
  width = MassDependentWidth(m, m0, gamma0, p, p0, ffr, lr)
  bw = RelativisticBreitWigner(m2, m0, width)
  ff = ffr*ffd
  if barrierFactor : 
    b1 = OrbitalBarrierFactor(p, p0, lr)
    b2 = OrbitalBarrierFactor(q, q0, ld)
    ff *= b1*b2
  return bw*Complex(ff, Const(0.))

def SubThresholdBreitWignerLineShape(m2, m0, gamma0, ma, mb, mc, md, dr, dd, lr, ld, barrierFactor = True) : 
  """
  Breit-Wigner amplitude (with the mass under kinematic threshold) 
  with Blatt-Weisskopf formfactors, mass-dependent width and orbital barriers
  """
  m = Sqrt(m2)
  mmin = ma+mb
  mmax = md-mc
  tanhterm = Tanh( (m0 - ( (mmin+mmax)/2.) )/(mmax-mmin) )
  m0eff = mmin + (mmax-mmin)*(1.+tanhterm)/2.
  q  = TwoBodyMomentum(md, m, mc)
  q0 = TwoBodyMomentum(md, m0eff, mc)
  p  = TwoBodyMomentum(m, ma, mb)
  p0 = TwoBodyMomentum(m0eff, ma, mb)
  ffr = BlattWeisskopfFormFactor(p, p0, dr, lr)
  ffd = BlattWeisskopfFormFactor(q, q0, dd, ld)
  width = MassDependentWidth(m, m0, gamma0, p, p0, ffr, lr)
  bw = RelativisticBreitWigner(m2, m0, width)
  ff = ffr*ffd
  if barrierFactor : 
    b1 = OrbitalBarrierFactor(p, p0, lr)
    b2 = OrbitalBarrierFactor(q, q0, ld)
    ff *= b1*b2
  return bw*Complex(ff, Const(0.))
  return bw*Complex(b1*b2*ffr*ffd, Const(0.))

def ExponentialNonResonantLineShape(m2, m0, alpha, ma, mb, mc, md, lr, ld, barrierFactor = True) : 
  """
  Exponential nonresonant amplitude with orbital barriers
  """
  m = Sqrt(m2)
  q  = TwoBodyMomentum(md, m, mc)
  q0 = TwoBodyMomentum(md, m0, mc)
  p  = TwoBodyMomentum(m, ma, mb)
  p0 = TwoBodyMomentum(m0, ma, mb)
  b1 = OrbitalBarrierFactor(p, p0, lr)
  b2 = OrbitalBarrierFactor(q, q0, ld)
  if barrierFactor : 
    return Complex(b1*b2*Exp(-alpha*(m2-m0**2)), Const(0.))
  else : 
    return Complex(Exp(-alpha*(m2-m0**2)), Const(0.))
