import tensorflow as tf

import sys, os
sys.path.append("../lib/")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from Interface import *
from Kinematics import *
from Dynamics import *
from Optimisation import *

from ROOT import TFile

if __name__ == "__main__" : 

  phsp = FourBodyAngularPhaseSpace()

  FL  = FitParameter("FL" ,  0.770,  0.000, 1.000, 0.01)
  AT2 = FitParameter("AT2",  0.200, -1.000, 1.000, 0.01)
  S5  = FitParameter("S5" , -0.100, -1.000, 1.000, 0.01)

  ### Start of model description

  def model(x) : 
    cosThetaK = phsp.CosTheta1(x)
    cosThetaL = phsp.CosTheta2(x)
    phi = phsp.Phi(x)

    sinThetaK = Sqrt( 1.0 - cosThetaK * cosThetaK )
    sinThetaL = Sqrt( 1.0 - cosThetaL * cosThetaL )

    sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
    sinTheta2L =  (1.0 - cosThetaL * cosThetaL)

    sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
    cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)

    pdf  = (3.0/4.0) * (1.0 - FL ) * sinTheta2K
    pdf +=  FL * cosThetaK * cosThetaK
    pdf +=  (1.0/4.0) * (1.0 - FL) * sin2ThetaK *  cos2ThetaL
    pdf +=  (-1.0) * FL * cosThetaK * cosThetaK *  cos2ThetaL
    pdf +=  (1.0/2.0) * (1.0 - FL) * AT2 * sinTheta2K * sinTheta2L * Cos(2.0 * phi )
    pdf +=  S5 * sin2ThetaK * sinThetaL * Cos( phi )

    return pdf

  ### End of model description

  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  init = tf.global_variables_initializer()
  sess = tf.Session()
  sess.run(init)

  norm_sample = sess.run( phsp.UniformSample(1000000) )
  majorant = EstimateMaximum(sess, model(data_ph), data_ph, norm_sample )*1.1
  print "Maximum = ", majorant

  data_sample = RunToyMC( sess, model(data_ph), data_ph, phsp, 10000, majorant, chunk = 1000000)

  norm = Integral( model(norm_ph) )
  nll = UnbinnedNLL( model(data_ph), norm )

  result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample } )
  print result
  WriteFitResults(result, "result.txt")

  fit_data = RunToyMC( sess, model(data_ph), data_ph, phsp, 1000000, majorant, chunk = 1000000)
  f = TFile.Open("toyresult.root", "RECREATE")
  FillNTuple("toy", fit_data, ["cos1", "cos2", "phi" ])
  f.Close()
